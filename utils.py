import os
import json
import requests
import datetime
import pandas as pd
import numpy as np
from tqdm import tqdm
from sklearn.preprocessing import MinMaxScaler
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import Matern, ExpSineSquared, WhiteKernel

from concurrent.futures import ThreadPoolExecutor, as_completed

# Function that loads the config
def load_config(alternative_config_file=None):
    '''
    Parameters
    ----------
    alternative_config_file : str
        The path to the config file. If None, the default config file is used.

    Returns
    -------
    config : dict
        A dictionary with the config file
    '''

    # Build the path to the config file
    if alternative_config_file is None:
        config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.json')
    else:
        # If the alternative path is relative, make it relative to the __file__
        if not os.path.isabs(alternative_config_file):
            config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), alternative_config_file)
        else:
            config_file = alternative_config_file

    # Read the paths json file
    with open(config_file, 'r') as f:
        config = json.load(f)
    
    # If the preprocessing batch size is 0, raise an error
    if config['PREPROCESSING_BATCH_SIZE'] == 0:
        raise ValueError('The preprocessing batch size must be greater than 0')

    # If any of the paths don't exist, create them
    for k, v in config.items():
        if "PATH" in k or "FILE" in k:
            # If the path or filepath is relative, make it absolute by adding the relative path to the config directory
            if not os.path.isabs(v):
                absolute_path = os.path.join(os.path.dirname(os.path.abspath(config_file)), v)
                config[k] = absolute_path
            else:
                absolute_path = v
        
        if "PATH" in k:
            # Make sure the path exists
            absolute_dir = os.path.dirname(absolute_path)
            if not os.path.exists(absolute_dir):
                os.makedirs(absolute_dir)
                print('Created path: ' + k)
        if "FILE" in k:
            # If the file does not exist, warn the user
            if not os.path.exists(absolute_path):
                print('Warning: File does not exist: ' + k)

    return config


# Function that makes a OpenWeather API call
def get_weather(start_date, end_date=None, cnt=None):
    # Read the config json file
    config = load_config()

    # Create the URL
    '''
    Format: http://history.openweathermap.org/data/2.5/history/city?lat={lat}&lon={lon}&type=hour&start={start}&end={end}&appid={API key}
    API Parameters
    lat, lon 	required 	Geographical coordinates (latitude, longitude). If you need the geocoder to automatic convert city names and zip-codes to geo coordinates and the other way around, please use our Geocoding API.
    type 	required 	Type of the call, keep this parameter in the API call as hour.
    appid 	required 	Your unique API key (you can always find it on your account page under the "API key" tab)
    start 	optional 	Start date (unix time, UTC time zone), e.g. start=1369728000
    end 	optional 	End date (unix time, UTC time zone), e.g. end=1369789200
    cnt 	optional 	A number of timestamps in response (one per hour, can be used instead of the parameter end)
    '''
    if cnt is None and end_date is not None:
        url = f"http://history.openweathermap.org/data/2.5/history/city?lat={config['LAT']}&lon={config['LON']}&type=hour&start={date_to_timestamp(start_date)}&end={date_to_timestamp(end_date)}&appid={config['KEY']}"
    elif end_date is None and cnt is not None:
        url = f"http://history.openweathermap.org/data/2.5/history/city?lat={config['LAT']}&lon={config['LON']}&type=hour&start={date_to_timestamp(start_date)}&cnt={cnt}&appid={config['KEY']}"
    else:
        raise ValueError('Either cnt or end must be specified')

    # Make the API call and return the response
    response = requests.get(url)

    if response.status_code == 200:
        return response
    else:
        raise ValueError(f'Error making API call: {response.status_code}')


# Function that converts an input date into a unix timestamp
def date_to_timestamp(date):
    '''
    Parameters
    ----------
    date : str or int
        The date to convert. String must be in the format 'YYYY-MM-DD'. Integer must be a unix timestamp.
    
    Returns
    -------
    timestamp : int
        The unix timestamp of the date
    '''
    if type(date) is str:
        try:
            timestamp = int(datetime.datetime.strptime(date, '%Y-%m-%d').timestamp())
        except ValueError as e:
            raise ValueError(f'Date must be in the format YYYY-MM-DD')
    elif type(date) in [int, np.int64]:
        timestamp = date
    else:
        raise ValueError(f'Date must be a string of the format YYYY-MM-DD or an integer timestamp but found {type(date)}')
    return timestamp


# Function that creates a weather dataframe given a start and end date
def create_weather_df(start_date, end_date):
    '''
    Parameters
    ----------
    start_date : str or int
        The start date of the dataframe. String must be in the format 'YYYY-MM-DD'. Integer must be a unix timestamp.
    end_date : str or int
        The end date of the dataframe. String must be in the format 'YYYY-MM-DD'. Integer must be a unix timestamp.
    config_file : str
        The path to the config file

    Returns
    -------
    weather_df : pandas.DataFrame
        A dataframe with the weather data
    '''

    # Get the weather data
    response_dict = dict(get_weather(date_to_timestamp(start_date), date_to_timestamp(end_date)).json())

    # Remove lists from the response dict
    for i in range(len(response_dict['list'])):
        if not len(response_dict['list'][i]['weather']) == 1:
            raise ValueError('List in response dict contains more than one weather measurement')
        response_dict['list'][i]['weather'] = response_dict['list'][i]['weather'][0]

    # Return the dataframe from the nested dictionary
    return pd.json_normalize(response_dict['list'])


# Function that completes the existing weather dataframe stored in the weather directory
def complete_weather_df(start_date, end_date, verbose=False):
    '''
    Parameters
    ----------
    start_date : str or int
        The start date of the dataframe. String must be in the format 'YYYY-MM-DD'. Integer must be a unix timestamp. If None, the start date will be the first date in the weather directory.
    end_date : str or int
        The end date of the dataframe. String must be in the format 'YYYY-MM-DD'. Integer must be a unix timestamp. If None, the end date will be the last date in the weather directory.
    config_file : str
        The path to the config file
    verbose : bool
        Whether to print the progress of the dataframe creation

    Returns
    -------
    weather_df : pandas.DataFrame
        A dataframe with the weather data
    '''
    # Convert the dates to timestamps
    # If either of the dates are None, ignore them
    if start_date is not None:
        start_timestamp = date_to_timestamp(start_date)
    if end_date is not None:
        end_timestamp = date_to_timestamp(end_date)

    # Load the config file
    config = load_config()

    # If the weather dataframe exists, load it
    if os.path.exists(config['PATH_WEATHER']):
        if verbose: print('Loading weather dataframe')
        stored_weather_df = pd.read_csv(config['PATH_WEATHER'])
        length_before_update = len(stored_weather_df)
        first_timestamp = stored_weather_df.iloc[0]['dt']
        last_timestamp = stored_weather_df.iloc[-1]['dt']

        # Check if the dataframe is complete
        # The measurements are updated every hour
        if start_date is not None and first_timestamp - start_timestamp > 3600:
            if verbose: print(f'Requested start date ({start_timestamp}) is more than one hour before the first stored date ({first_timestamp}). Updating weather dataframe')
            before_weather_df = create_weather_df(start_timestamp, first_timestamp)
            stored_weather_df = pd.concat([before_weather_df, stored_weather_df])

        if end_date is not None and end_timestamp - last_timestamp > 3600:
            if verbose: print(f'Requested end date ({end_timestamp}) is more than one hour after the last stored date ({last_timestamp}). Updating weather dataframe')
            after_weather_df = create_weather_df(last_timestamp, end_timestamp)
            stored_weather_df = pd.concat([stored_weather_df, after_weather_df])

        # Remove duplicate rows
        stored_weather_df = stored_weather_df.drop_duplicates()

        # Write the weather dataframe to the weather directory
        if length_before_update == len(stored_weather_df):
            if verbose: print('Weather dataframe is already up to date')
        else:
            if verbose: print(f'{len(stored_weather_df) - length_before_update} line(s) added. Writing weather dataframe')
            stored_weather_df.to_csv(config['PATH_WEATHER'], index=False)
            if verbose: print('Weather dataframe updated')
        return stored_weather_df

    # If the weather dataframe doesn't exist, create it
    else:
        if verbose: print('Creating weather dataframe')

        if start_date is None:
            if verbose: print('No start date specified. Using yesterday as the start date')
            start_timestamp = date_to_timestamp(int((datetime.datetime.now() - datetime.timedelta(days=1)).timestamp()))
        if end_date is None:
            if verbose: print('No end date specified. Using now as the end date')
            end_timestamp = date_to_timestamp(int(datetime.datetime.now().timestamp()))

        stored_weather_df = create_weather_df(start_timestamp, end_timestamp)
        stored_weather_df.to_csv(config['PATH_WEATHER'], index=False)

        if verbose: print('Weather dataframe created')
        return stored_weather_df


# Function that loads the weather dataframe
def load_weather_df(verbose=True):
    '''
    Parameters
    ----------
    config_file : str
        The path to the config file

    Returns
    -------
    weather_df : pandas.DataFrame
        A dataframe with the weather data
    '''
    # Load the config file
    config = load_config()

    # If the weather dataframe exists, load it
    if os.path.exists(config['PATH_WEATHER']):
        return pd.read_csv(config['PATH_WEATHER'])
    else:
        if verbose:
            print('Weather dataframe does not exist. Creating weather dataframe from the last monnth')
        start_date = int((datetime.datetime.now() - datetime.timedelta(days=30)).timestamp())
        end_date = int(datetime.datetime.now().timestamp())
        return complete_weather_df(start_date, end_date)


# Function that preprocesses the weather dataframe
def preprocess_weather_df(weather_df):
    '''
    Parameters
    ----------
    weather_df : pandas.DataFrame
        A dataframe with the weather data

    Returns
    -------
    weather_df : pandas.DataFrame
        The preprocessed weather dataframe
    '''
    # Convert all temperatures from Kelvin to Celsius
    for column in weather_df.columns:
        if 'temp' in column:
            weather_df[column] = weather_df[column] - 273.15
    weather_df['main.feels_like'] = weather_df['main.feels_like'] - 273.15

    # Compute the uncertainty from the max and min temperatures, assuming a 3 sigma confidence interval
    weather_df['main.temp_err'] = (weather_df['main.temp_max'] - weather_df['main.temp_min']) / 2 / 3

    return weather_df

# Function that fits a gaussian process to the temperature timeseries
def interpolate_temperature(weather_df, n_restarts_optimizer=16, kernel=None):
    if not 'main.temp_err' in weather_df.columns:
        raise ValueError('Weather dataframe does not contain the temperature uncertainty column. Please preprocess the weather dataframe first')

    # Extract the timestamp and temperature columns
    timestamp, temperature = weather_df['dt'], weather_df['main.temp']

    # MinMax scale the timestamp
    # To avoid the lengthscales becoming too small, the timestamp is scaled by a factor such that an interval of length 1 is equal to one day
    total_timespan_days = (timestamp.max() - timestamp.min()) / (60 * 60 * 24)
    min_max_scaler = MinMaxScaler(feature_range=(0, total_timespan_days))
    timestamp_scaled = min_max_scaler.fit_transform(timestamp.values.reshape(-1, 1))

    # Fit a gaussian process to the data
    if kernel is None:
        kernel = 1 * Matern()
    gp = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=n_restarts_optimizer, normalize_y=True, alpha=weather_df['main.temp_err']**2)
    # TODO Experiment with + 1 * ExpSineSquared(periodicity=1, periodicity_bounds='fixed') + WhiteKernel()
    gp.fit(timestamp_scaled, temperature)

    # Return the gaussian process and the minmax scaler
    return gp, min_max_scaler


# Function that returns the temperature at a given timestamp-array
def get_temperature(query_timestamp, gp:GaussianProcessRegressor, min_max_scaler:MinMaxScaler, chunk_size=1024, n_jobs=1):
    '''
    Parameters
    ----------
    query_timestamp : numpy.ndarray
        The timestamps at which the temperature should be predicted
    gp : GaussianProcessRegressor
        The fitted gaussian process
    min_max_scaler : MinMaxScaler
        The fitted minmax scaler
    chunk_size : int
        The size of the chunks used to predict the temperature
    n_jobs : int
        The number of jobs used to predict the temperature
    
    Returns
    -------
    temperature : numpy.ndarray
        The predicted temperature
    temperature_err : numpy.ndarray
        The predicted temperature uncertainty
    '''
    if not isinstance(query_timestamp, np.ndarray):
        query_timestamp = np.array(query_timestamp)
        
    # To avoid memory overflow, split the query_timestamp into chunks of size chunk_size
    query_timestamp_chunks_indices = np.array_split(np.arange(len(query_timestamp)), int(len(query_timestamp) / chunk_size) + 1)

    # Function that predicts the temperature for a given chunk
    def _get_temperature(query_timestamp_chunk_indices):
        # Define an empty array to store the temperatures
        temperature_chunk = np.empty(len(query_timestamp_chunk_indices))
        temperature_err_chunk = np.empty(len(query_timestamp_chunk_indices))

        # MinMax scale the timestamp
        query_timestamp_chunk_scaled = min_max_scaler.transform(query_timestamp[query_timestamp_chunk_indices].reshape(-1, 1))

        # Where the query_timestamp_chunk is within the range of the MinMaxScaler, return the temperature
        timestamp_inside_range_mask = (query_timestamp[query_timestamp_chunk_indices] >= min_max_scaler.data_min_[0]) & (query_timestamp[query_timestamp_chunk_indices] <= min_max_scaler.data_max_[0])
        if np.any(timestamp_inside_range_mask):
            temperature_chunk[timestamp_inside_range_mask], temperature_err_chunk[timestamp_inside_range_mask] = gp.predict(query_timestamp_chunk_scaled[timestamp_inside_range_mask], return_std=True)

        # Where the query_timestamp_chunk is outside the range of the MinMaxScaler, return NaN
        if np.any(~timestamp_inside_range_mask):
            temperature_chunk[~timestamp_inside_range_mask], temperature_err_chunk[~timestamp_inside_range_mask] = np.nan, np.nan

        return temperature_chunk, temperature_err_chunk

    if len(query_timestamp_chunks_indices) == 1:
        # If there is only one chunk, return the temperature
        return _get_temperature(query_timestamp_chunks_indices[0])

    # Otherwise, use multiprocessing to predict the temperature
    if n_jobs == -1:
        n_jobs = os.cpu_count()

    pbar = tqdm(total=len(query_timestamp_chunks_indices))
    pbar.set_description(f'Predicting Temperatures using {n_jobs} workers')
    with ThreadPoolExecutor(max_workers=n_jobs) as executor:
        results = []
        future_to_file = {executor.submit(_get_temperature, query_timestamp_chunk_indices): query_timestamp_chunk_indices for query_timestamp_chunk_indices in query_timestamp_chunks_indices}
        
        for future in as_completed(future_to_file):
            pbar.update(1)
            results.append(future.result())
            
    # Close the progress bar
    pbar.close()

    # Concatenate the results
    temperature = np.concatenate([result[0] for result in results])
    temperature_err = np.concatenate([result[1] for result in results])

    # Return the temperature and the temperature error
    return np.array(temperature), np.array(temperature_err)

# Function that computes the additive distribution of a set of measurements and uncertainties
def additive_distribution(measurements, uncertainties, resolution=1000, padding=None, normalize=True, xlim:tuple=None):
    if not isinstance(measurements, np.ndarray):
        measurements = np.array(measurements)
    if not isinstance(uncertainties, np.ndarray):
        uncertainties = np.array(uncertainties)

    if xlim is None:
        if padding is None:
            xlim = (measurements.min(), measurements.max())
        elif isinstance(padding, (int, float)):
            xlim = (measurements.min() - padding, measurements.max() + padding)
        elif isinstance(padding, tuple):
            xlim = (measurements.min() - padding[0], measurements.max() + padding[1])
        else:
            raise ValueError('padding must be either None, an int or a tuple')
    else:
        if padding is not None:
            raise ValueError('padding cannot be specified if xlim is specified')
        if xlim[0] is None:
            xlim = (measurements.min(), xlim[1])
        if xlim[1] is None:
            xlim = (xlim[0], measurements.max())

    total_distribution_linspace = np.linspace(*xlim, resolution)
    total_distribution = np.sum(np.exp(-((total_distribution_linspace[:, None] - measurements[None, :]) / uncertainties[None, :])**2) / (np.sqrt(2 * np.pi) * uncertainties[None, :]), axis=1)

    if normalize:
        total_distribution = total_distribution / np.sum(total_distribution * (total_distribution_linspace[1] - total_distribution_linspace[0]))

    return total_distribution_linspace, total_distribution