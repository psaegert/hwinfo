import matplotlib.pyplot as plt
from difflib import SequenceMatcher
from functools import cmp_to_key

BGCOLOR = plt.get_cmap('viridis')(0)
BGCOLOR = tuple(BGCOLOR[i]*0.7 for i in range(len(BGCOLOR)-1)) + (1,)

def dark_viridis():
    return BGCOLOR

def key_histogram(df, *ignore, key, bins=20, figsize=None, log=False):
    if ignore:
        raise TypeError("Too many non-keyword arguments.")
    if figsize is None:
        figsize = (12, 8)
    fig, ax = plt.subplots(figsize=figsize)
    ax.hist(df[key], bins=bins)
    ax.set_xlim(df[key].min(), df[key].max())
    ax.set_title(key)
    if log:
        ax.set_yscale('log')

    return fig, ax
    
# Function that takes a list of strings and a query string and returns the list of string ordered by the similarity to the query string
def order_by_similarity(list_of_strings, query_string):
    def sort_by_similarity(a, b):
        return SequenceMatcher(None, a, query_string).ratio() - SequenceMatcher(None, b, query_string).ratio()
    return sorted(list_of_strings, key=cmp_to_key(sort_by_similarity), reverse=True)

# Function that uses order_by_similarity to search for columns in a dataframe and returns the top n matches
def searchc(df, query_string, n=5):
    return order_by_similarity(list(df.columns), query_string)[:n]