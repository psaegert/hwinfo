import os
import sys
import json
import pandas as pd
import numpy as np
from tqdm import tqdm

from concurrent.futures import ThreadPoolExecutor, as_completed

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from utils import load_config, complete_weather_df

# Define colors
# Print information normally, warnings and errors in red and successes in green
RED = '\033[91m'
GREEN = '\033[92m'
RESET = '\033[0m'

# Define the cleaning procedure
# This has to be done on a file by file basis
def clean(input_path, output_path, n_workers=-1, verbose=True, use_cache=True):
    '''
    Clean the data
    
    Parameters
    ----------
    input_path : str
        Path to the directory containing the raw data
    output_path : str
        Path to the directory containing the preprocessed data
    n_workers : int
        Number of workers to use for the cleaning procedure
    verbose : bool
        Whether to print information about the cleaning procedure
    use_cache : bool
        Whether to use the cache to speed up the cleaning procedure
        
    Returns
    -------
    results : list
        List of the number of missing rows for each file
    '''

    if n_workers == -1:
        n_workers = os.cpu_count()

    if not use_cache:
        if verbose: print('Deleting cleaned data...')
        # Delete all cleaned files
        for filename in sorted(os.listdir(output_path)):
            os.remove(os.path.join(output_path, filename))

    # Check the preprocessing status
    last_cleaned_index = load_preprocessing_status()['LAST_CLEANED_INDEX'] - 2 if use_cache else 0

    # The last file could still have changed since the last preprocessing, so we need to clean it again

    # Get all the files in the input directory that have to be cleaned
    input_files = [os.path.join(input_path, f) for f in sorted(os.listdir(input_path))[last_cleaned_index:] if (f.endswith('.csv') or f.endswith('.CSV'))]

    # Get all the files in the output directory (same names as the input files but in the output directory)
    output_files = [os.path.join(output_path, f) for f in sorted(os.listdir(input_path))[last_cleaned_index:] if (f.endswith('.csv') or f.endswith('.CSV'))]

    # Define the cleaning step

    # Function that scans through a file, counting the number of columns in each row
    # If there are rows with different number of columns, add the missing columns names to the header of the csv file
    def _fill_missing_columns(input_file, output_file):
        # Read the rows of the file
        with open(input_file, 'r') as f:
            rows = f.readlines()
        
        # Find the maximum number of columns
        max_cols = 0
        for row in rows:
            row_cols = len(row.split(','))
            if row_cols > max_cols:
                max_cols = row_cols
        
        # Get the header of the file
        # Ignore the new line at the end of it (remove the last character)
        # The line ends with a comma, which is the last column, hence ignore the last column (remove the second last character)
        header = rows[0][:-2].split(',')

        # Add the missing columns to the header
        # Somehow, this creates one column too much, hence the -1
        n_missing_rows = max_cols - len(header) - 1
        for i in range(n_missing_rows):
            header.append('Unnamed:' + str(i + 10001))
                
        # Write the file with the new header into the output file
        # Skip the last two rows of the file, since they contain the footer
        with open(output_file, 'w', encoding='ISO-8859-1') as f:
            f.write(','.join(header))
            f.write(',\n')

            # Ignore the old header
            for row in rows[1:-2]:
                f.write(row)

        return n_missing_rows

    # Run the cleaning step on all the files
    pbar = tqdm(total=len(input_files)) if verbose else None
    if verbose: pbar.set_description(f'Cleaning files using {n_workers} workers')
    with ThreadPoolExecutor(max_workers=8) as executor:
        results = []
        future_to_file = {executor.submit(_fill_missing_columns, input_file, output_file): output_file for input_file, output_file in zip(input_files, output_files)}
        
        for future in as_completed(future_to_file):
            if verbose: pbar.update(1)
            results.append(future.result())

    if verbose: pbar.close()

    # Update the preprocessing status
    update_preprocessing_status('LAST_CLEANED_INDEX', last_cleaned_index + len(input_files))

    return results

# Define the preprocessing procedure
# This can be done in memory
def preprocess(input_path, n_workers=-1, verbose=True, use_cache=True):
    '''
    Preprocess the data

    Parameters
    ----------
    input_path : str
        Path to the directory containing the raw data
    n_workers : int
        Number of workers to use for the preprocessing procedure
    verbose : bool
        Whether to print information about the preprocessing procedure
    use_cache : bool
        Whether to use the cache to speed up the preprocessing procedure

    Returns
    -------
    df : pandas.DataFrame
        Preprocessed data
    results : list
        List of the preprocessing results of each file and step
    '''

    config = load_config()
    last_preprocessed_index = load_preprocessing_status()['LAST_PREPROCESSED_INDEX'] - 2 if use_cache else 0

    if n_workers == -1:
        n_workers = os.cpu_count()
        
    input_files_list = [os.path.join(input_path, f) for f in sorted(os.listdir(input_path)) if (f.endswith('.csv') or f.endswith('.CSV'))]

    # If the batch size changed, we need to preprocess all data again
    # If not, we can just preprocess the new files
    if config['PREPROCESSING_BATCH_SIZE'] != load_preprocessing_status()['PREPROCESSING_BATCH_SIZE'] or not use_cache:
        last_preprocessed_batch_index = 0
        # Delete all the final data
        if verbose: print('Deleting all preprocessed data')
        for f in os.listdir(config['PATH_FINAL']):
            if f.endswith('.csv') or f.endswith('.CSV'):
                os.remove(os.path.join(config['PATH_FINAL'], f))

        if config['PREPROCESSING_BATCH_SIZE'] != load_preprocessing_status()['PREPROCESSING_BATCH_SIZE']:
            if verbose: print(f'Batch size changed from {load_preprocessing_status()["PREPROCESSING_BATCH_SIZE"]} to {config["PREPROCESSING_BATCH_SIZE"]}. Preprocessing all data again.')
            update_preprocessing_status('PREPROCESSING_BATCH_SIZE', config['PREPROCESSING_BATCH_SIZE'])
    else:
        last_preprocessed_batch_index = last_preprocessed_index // config['PREPROCESSING_BATCH_SIZE']

    # Get the files to preprocess
    input_files_list = input_files_list[last_preprocessed_batch_index * config['PREPROCESSING_BATCH_SIZE']:]

    # Global dummy variable 
    df_list = [None for _ in range(len(input_files_list))]

    # Define the preprocessing steps

    # 1. Load the data from the csv file and filters our unnamed columns
    def _load_raw_file(file_index):
        # The file contains characters not included in UTF-8
        # Assume that the footer has already been removed
        df = pd.read_csv(input_files_list[file_index], encoding='ISO-8859-1')

        # If the dataframe contains unnamed columns, rename them
        # Scan for columns with 'Unnamed' in the name
        cols_removed = []
        for col in df.columns:
            if 'Unnamed' in col:
                # Remove the column
                df.drop(col, axis=1, inplace=True)
                cols_removed.append(col)
        
        return df, cols_removed

    # 2. Convert the 'Date' and 'Time' columns into a single 'DateTime' column and convert the datetime into a timestamp
    def _preprocess_timestamps(df_index):
        try:
            # Combine the 'Date' and 'Time' columns into a single 'DateTime' column and convert the datetime into a timestamp
            df_list[df_index]['DateTime'] = pd.to_datetime(df_list[df_index]['Date'] + ' ' + df_list[df_index]['Time'], format='%d.%m.%Y %H:%M:%S.%f')

            # Convert the 'DateTime' column into a timestamp
            df_list[df_index]['Timestamp'] = df_list[df_index]['DateTime'].apply(lambda x: x.timestamp())

            # Delete the 'DateTime', 'Date' and 'Time' columns
            df_list[df_index] = df_list[df_index].drop(columns=['DateTime', 'Date', 'Time'])
        except Exception as e:
            return e
        return True

    # 3. Convert the 'Yes'/'No' values to 1/0
    def _convert_yes_no(df_index):
        try:
            # Find all columns with 'Yes' or 'No' values
            cols_yes_no = df_list[df_index].columns[df_list[df_index].isin(['Yes', 'No']).any(axis=0)]

            # Convert the 'Yes' and 'No' values to 1 and 0
            for col in cols_yes_no:
                df_list[df_index][col] = df_list[df_index][col].apply(lambda x: 1 if x == 'Yes' else 0)

            # Assert that all types in the df are int64 or float64
            assert np.all([df_list[df_index][col].dtype in [np.int64, np.float64] for col in df_list[df_index].columns])

        except Exception as e:
            return e
        return True

    # 4. Remove duplicates
    def _remove_duplicates(df_index):
        try:
            # Remove duplicates
            df_list[df_index] = df_list[df_index].drop_duplicates()
        except Exception as e:
            return e
        return True

    # Do all steps for one file
    def _preprocess_file(index):
        # 1. Load the data from the csv file and store it in the df_list
        df_list[index], result_load = _load_raw_file(index)

        # 2. Convert the 'Date' and 'Time' columns into a single 'DateTime' column and convert the datetime into a timestamp
        result_timestamp = _preprocess_timestamps(index)

        # 3. Convert the 'Yes'/'No' values to 1/0
        result_yesno = _convert_yes_no(index)

        # 4. Remove duplicates
        result_duplicates = _remove_duplicates(index)

        return result_load, result_timestamp, result_yesno, result_duplicates

    # Run the preprocessing steps
    pbar = tqdm(total=len(df_list)) if verbose else None
    if verbose: pbar.set_description(f'Preprocessing files using {n_workers} workers')
    with ThreadPoolExecutor(max_workers=8) as executor:
        results = []
        future_to_file = {executor.submit(_preprocess_file, index): index for index in range(len(df_list))}
        
        for future in as_completed(future_to_file):
            if verbose: pbar.update(1)
            results.append(future.result())
    
    # Close the progress bar
    if verbose: pbar.close()

    # Update the preprocessing status
    update_preprocessing_status('LAST_PREPROCESSED_INDEX', last_preprocessed_batch_index * config['PREPROCESSING_BATCH_SIZE'] + len(input_files_list))
    
    # Combine and save the dataframes into batches with config['PREPROCESSING_BATCH_SIZE'] dataframes
    if verbose: print('Saving dataframe fragments...')
    df_fragment = pd.DataFrame()
    pbar = df_list
    if verbose:
        pbar = tqdm(pbar)
        pbar.set_description('Saving')
    for i, df in enumerate(pbar):
        df_fragment = pd.concat([df_fragment, df], axis=0, ignore_index=True)

        if i % config['PREPROCESSING_BATCH_SIZE'] == 0 and i != 0:
            # Save the dataframe
            df_fragment.to_csv(os.path.join(config['PATH_FINAL'], f'preprocessed_batch_{i // config["PREPROCESSING_BATCH_SIZE"] + last_preprocessed_batch_index}.csv'), index=False)
            df_fragment = pd.DataFrame()
    else:
        # Save the dataframe if it is not empty
        if not df_fragment.empty:
            df_fragment.to_csv(os.path.join(config['PATH_FINAL'], f'preprocessed_batch_{int(np.ceil(i / config["PREPROCESSING_BATCH_SIZE"])) + last_preprocessed_batch_index}.csv'), index=False)

    
    return results


def finalize(df, complete_weather:bool=True, save:bool=False, verbose:bool=False):
    '''
    Finalize the preprocessing.

    Parameters
    ----------
    df : pd.DataFrame
        The dataframe to be processed.
    complete_weather : bool
        Whether to complete the weather data.
    save : bool
        Whether to save the dataframe.
    verbose : bool
        Whether to print the progress.

    Returns
    -------
    pd.DataFrame
        The final dataframe.
    '''
    config = load_config()

    # Check if the 'Timestamp' column is sorted
    # If not, sort it
    time1 = pd.Timestamp.now()
    if not df['Timestamp'].is_monotonic_increasing:
        if verbose: print('The "Timestamp" column is not sorted. Sorting...')
        df = df.sort_values(by='Timestamp')
        if verbose: print(GREEN + f'Sorting done. ({(pd.Timestamp.now() - time1).total_seconds():.1f}s)' + RESET)
    else:
        if verbose: print(GREEN + f'The "Timestamp" column is sorted.' + RESET)


    # All further processing is done on the concatenated dataframe
    # Hence, reset the index of the dataframe
    time1 = pd.Timestamp.now()
    if verbose: print('Resetting index...')
    df = df.reset_index(drop=True)
    if verbose: print(GREEN + f'Index reset. ({(pd.Timestamp.now() - time1).total_seconds():.1f}s)' + RESET)


    # Find columns in which there are nan values
    df_nan = df.isnull().any()
    nan_columns = df_nan[df_nan].index
    new_line = '\n'
    if verbose: print(f'{len(nan_columns)} columns have nan values:\n- {f"{new_line}- ".join(nan_columns)}')

    # Drop nan columns
    df.drop(columns=nan_columns, inplace=True)
    if verbose: print(GREEN + f'Dropped {len(nan_columns)} columns with nan values' + RESET)


    # Rename the fan columns according to their position in the case
    position_dict =  {
        'AUXFANIN0 [RPM]': 'SYS Fan Front [RPM]',
        'AUXFANIN1 [RPM]': 'SYS Fan Bottom [RPM]',
        'AUXFANIN2 [RPM]': 'SYS Fan Rear [RPM]',
        'Fan [RPM]': 'CPU Fan Top [RPM]',
        'Pump [RPM]': 'CPU Pump [RPM]',
        'CPUFANIN0 [RPM]': 'CPU Pump Power'
    }
    df.rename(columns=position_dict, inplace=True)

    # Check that the relevant columns have been renamed in the df
    if verbose:
        for old, new in position_dict.items():
            if not new in df.columns:
                print(RED + f'Could not rename {old} to {new}. New column not found in df.' + RESET)


    # Compute the time difference between the timestamps
    df['Interval'] = df['Timestamp'].diff()


    # Delete the first row, since the Interval column is not defined for the first row
    df.drop(0, inplace=True)

    # Check that no nan values are left in the Interval column
    if df['Interval'].isnull().any():
        # Print the error message in red
        if verbose: print(RED + 'The Interval column still contains nan values.' + RESET)
    else:
        if verbose: print(GREEN + 'No nan values in the Interval column.' + RESET)


    # Save the dataframe to a csv file
    if save:
        if verbose: print('Saving final dataframe to csv...')
        time1 = pd.Timestamp.now()
        df.to_csv(config["FILE_FINAL"], index=False)
        if verbose: print(GREEN + f'Saving done. ({os.path.getsize(config["FILE_FINAL"]) *  1e-6:.1f}MB in {(pd.Timestamp.now() - time1).total_seconds():.1f}s)' + RESET)


    # Update the weather data
    if complete_weather:
        if verbose: print('Updating weather data...')
        time1 = pd.Timestamp.now()
        # Consider an additional day of data before the first measurement
        additional_time_before_first_measurement = 3600 * 24
        complete_weather_df(int(df['Timestamp'].iloc[0]) - additional_time_before_first_measurement, int(time1.timestamp()), verbose=verbose)
        if verbose: print(GREEN + f'Weather data updated. ({(pd.Timestamp.now() - time1).total_seconds():.1f}s)' + RESET)

    return df

# Function that adds leading zeros to filenames
def add_leading_zeros(path, verbose=False):
    '''
    Add leading zeros to filenames.

    Parameters
    ----------
    path : str
        The path to the folder containing the files.
    verbose : bool
        Whether to print the progress.

    Returns
    -------
    max_n_digits_changed : bool
        Whether the maximum number of digits has changed.
    '''
    # Check if all the files in the input_path have enough leading zeros
    if verbose: print('Checking if all the files have enough leading zeros...')
    input_filenames = sorted(os.listdir(path))

    # Find the number of leading zeros by finding the maximum length of all filenames. Strip the filenames from their extensions.
    max_n_digits = max([len(os.path.splitext(filename)[0]) for filename in input_filenames])

    # Ensure all the files have enough leading zeros
    if verbose: print('Ensuring all the files have enough leading zeros...')
    for filename in input_filenames:
        # Get the number of leading zeros
        n_digits = len(os.path.splitext(filename)[0])

        # Check if the file has enough leading zeros
        if n_digits < max_n_digits:
            # Add leading zeros to the filename
            new_filename = '0' * (max_n_digits - n_digits) + filename

            # Rename the file
            os.rename(os.path.join(path, filename), os.path.join(path, new_filename))

    
    # Read the last max_n_digits from the status
    last_max_n_digits = load_preprocessing_status()['MAX_N_DIGITS']

    if max_n_digits != last_max_n_digits:
        if verbose: print(f'The maximum number of digits has changed from {last_max_n_digits} to {max_n_digits}.')

        # Update the status
        update_preprocessing_status(max_n_digits=max_n_digits)

        # Return that the max_n_digits has changed
        return True
    
    return False

# Function that builds the dataframe and combines the pieces
def build_df(complete_weather=True, save=False, verbose=False, use_cache=True, lazy=False):
    '''
    Build the dataframe.

    Parameters
    ----------
    complete_weather : bool
        Whether to complete the weather data.
    save : bool
        Whether to save the dataframe.
    verbose : bool
        Whether to print the progress.
    use_cache : bool
        Whether to use the cache.
    lazy : bool
        Whether to skip cleaning and preprocessing.

    Returns
    -------
    pd.DataFrame
        The final dataframe.
    '''
    config = load_config()

    if not lazy:
        # Add leadaing zeros to the raw filenames
        max_n_digits_changed = add_leading_zeros(config["PATH_RAW"], verbose=verbose)

        # Clean the data
        _ = clean(config['PATH_RAW'], config['PATH_CLEANED'], verbose=verbose, use_cache=(use_cache and not max_n_digits_changed))

        # Preprocess the data
        _ = preprocess(config['PATH_CLEANED'], verbose=verbose, use_cache=use_cache)

    # Concatenate the dataframes
    if verbose: print('Reading dataframe fragments...')
    df = pd.DataFrame()
    pbar = sorted(os.listdir(config['PATH_FINAL']))
    if verbose:
        pbar = tqdm(pbar)
        pbar.set_description('Reading')
    for filename in pbar:
        df = pd.concat([df, pd.read_csv(os.path.join(config['PATH_FINAL'], filename))], axis=0, ignore_index=True)

    # Finalize the data
    return finalize(df, complete_weather=complete_weather, save=save, verbose=verbose)


# Function that loads the status of preprocessing
def load_preprocessing_status():
    config = load_config()

    # Check if the preprocessing status file exists
    if os.path.isfile(config['FILE_PREPROCESSING_STATUS']):
        # Load the preprocessing status file
        with open(config['FILE_PREPROCESSING_STATUS'], 'r') as f:
            status = json.load(f)
    else:
        # Create the preprocessing status file
        status = {
            'LAST_CLEANED_INDEX': 0,
            'LAST_PREPROCESSED_INDEX': 0,
            'PREPROCESSING_BATCH_SIZE': config['PREPROCESSING_BATCH_SIZE'],
            'MAX_N_DIGITS': 1
        }
        with open(config['FILE_PREPROCESSING_STATUS'], 'w') as f:
            json.dump(status, f)

    return status

# Function that updates the status of preprocessing
def update_preprocessing_status(key, value):
    config = load_config()

    # Load the preprocessing status file
    with open(config['FILE_PREPROCESSING_STATUS'], 'r') as f:
        status = json.load(f)

    # Update the status
    status[key] = value

    # Save the status
    with open(config['FILE_PREPROCESSING_STATUS'], 'w') as f:
        json.dump(status, f)
